package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class LoginController {

    @FXML
    private Button cancelButton;
    @FXML
    private Button loginButton;
    @FXML
    private Label loginMessageLabel;
    @FXML
    private Label LabelSucess;
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField enterPasswordField;


    public void loginButtonOnAction(ActionEvent event){

        if (usernameTextField.getText().isBlank() == false && enterPasswordField.getText().isBlank() == false) {
            LabelSucess.setText("Login Concluído com Sucesso!");
            loginMessageLabel.setText("");
        } else {
            loginMessageLabel.setText("Digite seu usuário e senha!");
            LabelSucess.setText("");
        }

    }


    public void cancelButtonOnAction(ActionEvent event){
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    public void validateLogin(){
        DatabaseConnection connectNow = new DatabaseConnection();
        Connection connectDB = connectNow.getConnection();

        String verifyLogin = "SELECT count(1) FROM user_account WHERE username = ''" + usernameTextField.getText() + "'AND password ='" + enterPasswordField.getText() +"''";

        try{

            Statement statement = connectDB.createStatement();
            ResultSet queryResult = statement.executeQuery(verifyLogin);

            while(queryResult.next()){
                if(queryResult.getInt(1) == 1){
                    loginMessageLabel.setText("parabens!");
                }else{
                    loginMessageLabel.setText("Login inválido, tente novamente") ;

                }
            }

        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }
    }

}