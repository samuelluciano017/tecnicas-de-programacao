package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class Main extends Application {


    @FXML
    private Button BTN1;
    @FXML
    private PasswordField PassId;
    @FXML
    private TextField Field;
    @FXML
    private Label resposta;


    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        if (Field.getText().toString().equals("aluno") && PassId.getText().toString().equals("12345")){
            resposta.setText("Login efetuado com sucesso!!!");


        }else{
            resposta.setText("Credenciais incorretas");
        }

    }


    public static void main(String[] args) {
        launch(args);
    }
}
