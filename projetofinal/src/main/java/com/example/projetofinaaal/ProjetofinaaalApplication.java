package com.example.projetofinaaal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetofinaaalApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetofinaaalApplication.class, args);
    }

}
