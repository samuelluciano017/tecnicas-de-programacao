package com.example.projetofinaaal;

public @interface Size {
    int min();
}
