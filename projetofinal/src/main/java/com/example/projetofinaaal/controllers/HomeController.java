package com.example.projetofinaaal.controllers;

import com.example.projetofinaaal.Ninja;
import com.example.projetofinaaal.projetoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {


    @RequestMapping("/login")
    public String login(){

        return"login";
    }
    @RequestMapping("/form")
    public String index(){

        return"index";
    }


    @Autowired

    com.example.projetofinaaal.projetoRepository projetoRepository;

    @RequestMapping("/")
    public String listaUsuarios(Model model) {
        model.addAttribute("usuarios", projetoRepository.findAll());
        return "loginForm";
    }
    @GetMapping("/add")
    public String usuarioForm(Model model){
        model.addAttribute("ninja", new Ninja());
        return "registroForm";

    }

    @PostMapping("/process")
    public String processForm(@Validated Ninja ninja, BindingResult result){
        if(result.hasErrors()){
            return "registroForm";
        }
        projetoRepository.save(ninja);
        return "redirect:/";
    }

}

